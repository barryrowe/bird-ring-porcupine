using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Data;

namespace Editor
{
    public class CustomMenuActions: MonoBehaviour
    {
        private const int FieldID = 0;
        private const int FieldName = 1;
        private const int FieldDeficiency = 2;
        private const int FieldInRhyme = 3;
        private const int FieldOutRhyme = 4;
        private const int FieldKey = 5;

        private const string ChoicesBaseAssetPath = "Assets/Data/";
        private static readonly string ChoiceSetAssetPath = $"{ChoicesBaseAssetPath}ChoiceSet.asset";

        [MenuItem("RCG/Data/ImportCSV")]
        public static void ImportCsv()
        {

            var existingChoiceSet = AssetDatabase.LoadAssetAtPath<ChoiceSet>(ChoiceSetAssetPath);

            var csvFilePath = EditorUtility.OpenFilePanel("Open Choices CSV", "", "csv");
            
            if (csvFilePath == null || csvFilePath.Trim() == "") return;
            
            
            if (existingChoiceSet == null)
            {
                Debug.LogWarning("No 'ChoiceSet.asset' Found. Creating empty item.");
                existingChoiceSet = ScriptableObject.CreateInstance<ChoiceSet>();
                existingChoiceSet.choices = new ChoiceInfo[0];
                AssetDatabase.CreateAsset(existingChoiceSet, ChoiceSetAssetPath);
                
                EditorUtility.SetDirty(existingChoiceSet);
                AssetDatabase.SaveAssets();
            }

            var existingChoicesMap = existingChoiceSet != null
                ? existingChoiceSet.choices.ToDictionary(c => c.id)
                : new Dictionary<int, ChoiceInfo>();
            
            var lines = System.IO.File.ReadAllLines(csvFilePath);

            var updatedChoices = new List<ChoiceInfo>();

            var choicesMissingTextures = new List<string>();
            
            // Assume first line is headers
            for (var i = 1; i < lines.Length; i++)
            {
                var line = lines[i];
                var fields = line.Trim().Split(',');
                var id = int.Parse(fields[FieldID].Trim());
                var name = fields[FieldName].Trim();
                var deficiency = fields[FieldDeficiency].Trim();
                var inRhyme = fields[FieldInRhyme].Trim();
                var outRhyme = fields[FieldOutRhyme].Trim();
                var key = fields[FieldKey].Trim();

                var targetChoice = existingChoicesMap.ContainsKey(id) ? existingChoicesMap[id] : null;

                var fileName = $"{id}{outRhyme}-{key}";
                if (targetChoice == null)
                {
                    targetChoice = ScriptableObject.CreateInstance<ChoiceInfo>();
                    AssetDatabase.CreateAsset(targetChoice, $"{ChoicesBaseAssetPath}{fileName}.asset");
                }
                targetChoice.id = id;
                targetChoice.choiceName = name;
                targetChoice.deficiency = deficiency;
                targetChoice.inRhyme = inRhyme;
                targetChoice.outRhyme = outRhyme;
                targetChoice.key = key;
                targetChoice.name = fileName;

                var targetTexture = CachedResource.Load<Texture2D>($"Sprites/{key}");
                if (targetTexture == null)
                {
                    choicesMissingTextures.Add($"{id}-{key}");
                }

                targetChoice.texture = targetTexture;
                
                EditorUtility.SetDirty(targetChoice);
                AssetDatabase.SaveAssets();
                
                updatedChoices.Add(targetChoice);
            }

            existingChoiceSet.choices = updatedChoices.ToArray();
            EditorUtility.SetDirty(existingChoiceSet);
            AssetDatabase.SaveAssets();

            if (choicesMissingTextures.Count <= 0) return;

            var message = choicesMissingTextures.Aggregate("", (prev, curr) => $"{prev}\n{curr}");
            EditorUtility.DisplayDialog("Incomplete Content", message, "Got It!");
            // TODO: Find all ChoiceDriverComponents and fix Reference to ChoiceSet
        }
        
    }
}