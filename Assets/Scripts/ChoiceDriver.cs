using System.Collections.Generic;

public class ChoiceDriver
{
    private readonly ChoicePool _pool;
    private readonly List<Choice> _choices;
    private List<Choice> _nextOptions;

    public ChoiceDriver(ChoicePool pool)
    {
        _pool = pool ?? new ChoicePool();
        var rootChoice = _pool.GetRootChoice();
        _choices = new List<Choice>();
        _nextOptions = new List<Choice>();
        MakeChoice(rootChoice);
    }

    public Choice[] CurrentChoices()
    {
        return _choices.ToArray();
    }

    public Choice[] NextOptions()
    {
        return _nextOptions.ToArray();
    } 

    public void MakeChoice(Choice choice)
    {
        _pool.MarkChosen(choice);
        _choices.Add(choice);
        _nextOptions.Clear();
        _nextOptions.AddRange(_pool.GetNextOptions(choice));
    }
}