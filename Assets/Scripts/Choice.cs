using UnityEngine;

public class Choice
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Deficiency { get; set; }
    public string InRhyme { get; set; }
    public string OutRhyme { get; set; }
    public string Key { get; set; }
    public Texture2D Texture { get; set; }
}