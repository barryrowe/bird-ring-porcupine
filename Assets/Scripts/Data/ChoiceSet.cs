using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "AllChoices", menuName = "Choices/ChoiceSet", order = 1)]
    public class ChoiceSet: ScriptableObject
    {
        public ChoiceInfo[] choices;

        public List<Choice> ToChoices()
        {
            return choices.Select(c => new Choice()
            {
                Id = c.id,
                Name = c.choiceName,
                Deficiency = c.deficiency,
                InRhyme = c.inRhyme,
                OutRhyme = c.outRhyme,
                Key = c.key,
                Texture = c.texture

            }).ToList();
        }
    }
}