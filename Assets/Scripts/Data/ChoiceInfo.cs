using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "Choice", menuName = "Choices/Choice", order = 2)]
    public class ChoiceInfo: ScriptableObject
    {
        public int id;
        public string choiceName;
        public string deficiency;
        public string inRhyme;
        public string outRhyme;
        public string key;
        public Texture2D texture;
    }
}