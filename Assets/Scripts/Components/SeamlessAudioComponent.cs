using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Components
{
    public class SeamlessAudioComponent : MonoBehaviour
    {
        [SerializeField] private UnityEvent<int, string> whenClipStarted;
        [SerializeField] private AudioSource sourceA;
        [SerializeField] private AudioSource sourceB;

        [SerializeField] private AudioClip[] clips;
        [SerializeField] private bool shouldLoop;

        private List<double> _durations;
        private const double Delay = 0.1;

        private AudioSource[] _sources;
        private int _nextSource;
        private double _startTime;
        private int _nextClip;
        private double _nextStartTime;
        private double _triggerTime;
        private int _currentClip;

        public void SetNewClips(AudioClip[] newClips)
        {
            clips = newClips;
            Initialize();
        }

        private void Awake()
        {
            _sources = new[] { sourceA, sourceB };
            Initialize();
        }

        private void Update()
        {
            // We process the actual transition time first, because we short-circuit below if
            // the DSP time is not within the next clip's start time. 
            if (_currentClip >= 0 && _currentClip < clips.Length && _triggerTime > 0.0 && AudioSettings.dspTime >= _triggerTime)
            {
                var currentClip = clips[_currentClip];
                whenClipStarted.Invoke(_currentClip, currentClip.name);
                _triggerTime = _nextStartTime;
                _currentClip = _nextClip;
            }

            // If we do not have any clips to queue,or our current clip is not close to finishing
            // we do not need to do anything else, and we can stop processing until next frame.
            if (clips.Length == 0 || _nextClip >= clips.Length || !(AudioSettings.dspTime > _nextStartTime - 1)) return;
            
            var clipToPlay = clips[_nextClip];

            _sources[_nextSource].clip = clipToPlay;
            _sources[_nextSource].PlayScheduled(_nextStartTime);

            _nextStartTime += _durations[_nextClip];
            _nextClip += 1;
            _nextSource = 1 - _nextSource;


            if (shouldLoop && _nextClip >= clips.Length)
            {
                _nextClip = 0;
            }
        }

        private void Initialize()
        {
            _startTime = AudioSettings.dspTime + Delay;
            
            CalculateDurations();
            InitializeClips();
        }

        private void InitializeClips()
        {
            if (clips.Length <= 0) return;

            sourceA.clip = clips[0];
            var duration = _durations[0];
            sourceA.PlayScheduled(_startTime);
            _nextSource = 1;
            _nextClip = 1;
            _nextStartTime = _startTime + duration;
            _triggerTime = _startTime;
            _currentClip = 0;
        }

        private void CalculateDurations()
        {
            _durations = clips.Select(c => (double) c.samples / c.frequency).ToList();
        }
    }
}