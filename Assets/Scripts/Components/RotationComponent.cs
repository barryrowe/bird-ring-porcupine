using System;
using UnityEngine;

namespace Components
{
    public class RotationComponent: MonoBehaviour
    {
        [SerializeField] public float minRotation;
        [SerializeField] public float maxRotation;
        [SerializeField] public float rotationSpeed;

        private bool _isRotatingRight;
        private float _rotation;

        private void Start()
        {
            _rotation = transform.localEulerAngles.z;
        }


        private void Update()
        {
            var rangeMagnitude = rotationSpeed * Time.deltaTime;
            var rotationAmount = _isRotatingRight ? rangeMagnitude : rangeMagnitude * -1;
            var transform1 = transform;

            _rotation = Math.Clamp(_rotation + rotationAmount, minRotation, maxRotation);
            transform1.localEulerAngles = new Vector3(0, 0, _rotation);
            
            if ((_isRotatingRight && _rotation >= maxRotation) || (!_isRotatingRight && _rotation <= minRotation)) {
                _isRotatingRight = !_isRotatingRight;
            }
        }
    }
}