using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Components
{
    public class PlayButtonComponent: MonoBehaviour
    {
        [SerializeField] private ChoiceDriverComponent choiceDriverComponent;
        [SerializeField] private SeamlessAudioComponent seamlessPlayer;

        private void OnMouseUpAsButton()
        {
            var choices = choiceDriverComponent.GetChosenList();
            var introClip = CachedResource.Load<AudioClip>("Audio/clip-1");
            var clips = new List<AudioClip> {introClip};

            var choiceClips = choices
                .Select(c => CachedResource.Load<AudioClip>("Audio/choice-" + c.Key))
                .Where(c => c != null);
            
            clips.AddRange(choiceClips);
            
            // TODO: Add Exit Clip

            seamlessPlayer.SetNewClips(clips.ToArray());
        }
    }
}