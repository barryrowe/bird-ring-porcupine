using UnityEngine;
using UnityEngine.UI;

namespace Components
{
    public class ChoiceOptionComponent: MonoBehaviour
    {
        private SpriteRenderer _spriteRenderer;
        private Image _image;
        private Choice _choice;
        private Text _text;
        private bool _isSpriteRendererNotNull;
        private bool _isImageNotNull;
        private bool _hasTextComponent;
        private bool _choiceChanged = true;
        private bool _choiceHasTexture;

        private OnChosenDelegate _onChosenCallback;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _image = GetComponent<Image>();
            _text = GetComponentInChildren<Text>();
        }
        
        private void Start()
        {
            _isSpriteRendererNotNull = _spriteRenderer != null;
            _isImageNotNull = _image != null;
            _hasTextComponent = _text != null;
        }

        private void Update()
        {
            if (_choice == null ||
                (!_isSpriteRendererNotNull && !_isImageNotNull) ||
                !_choiceChanged
            ) return;

            
            var newSprite = _choiceHasTexture ? Sprite.Create(
                _choice.Texture,
                new Rect(0, 0, _choice.Texture.width, _choice.Texture.height),
                new Vector2(0.5f, 0.5f)
            ) : null;

            if (_isSpriteRendererNotNull)
            {
                _spriteRenderer.sprite = newSprite;
            }
            else if (_isImageNotNull)
            {
                _image.sprite = newSprite;
            }
            

            if (_hasTextComponent)
            {
                _text.text = _choice.Name;
            }
            
            _choiceChanged = false;
        }

        public void OnMouseUpAsButton()
        {
            Debug.Log("MouseUpAsButton");
            if (_onChosenCallback == null || _choice == null) return;

            _onChosenCallback(_choice);
        }

        public void SetChoice(Choice choice)
        {
            _choice = choice;
            _choiceChanged = true;
            _choiceHasTexture = _choice.Texture != null;
        }

        public Choice GetChoice()
        {
            return _choice;
        }

        public void SetOnChoiceSelected(OnChosenDelegate callback)
        {
            _onChosenCallback = callback;
        }
        
    }
    
    public delegate void OnChosenDelegate(Choice choice);
}