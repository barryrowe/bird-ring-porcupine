using System.Collections.Generic;
using System.Linq;
using Data;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Components
{
    public class ChoiceDriverComponent : MonoBehaviour
    {
        
        [FormerlySerializedAs("choices")] [SerializeField] private GameObject[] choiceButtons;
        [SerializeField] private ChoiceSet choiceSet;
        [SerializeField] private GameObject imagePrefab;

        private readonly List<ChoiceOptionComponent> _options = new List<ChoiceOptionComponent>();

        private ChoiceDriver _driver;
        private List<GameObject> _chainItems = new List<GameObject>();

        public List<Choice> GetChosenList()
        {
            return _driver.CurrentChoices().ToList();
        }

        public void AnimateChoiceForAudio(int clipPosition, string clipName)
        {
            // We need to adjust the position to account for any "intro" clips
            // before our chain clips start. (ie. "hush little baby...")
            var chainPosition = clipPosition - Constants.IntroClipCount;
            if (chainPosition < 0) return; // cannot play a chain position below 0
            if (chainPosition >= _chainItems.Count) return; // cannot animate an item not present
            
            // TODO: Update to run the target animation for a choice instead of just rotating the
            //       choice icon.
            foreach (var chainItem in _chainItems)
            {
                var rotator = chainItem.GetComponent<RotationComponent>();
                if (rotator != null) { Destroy(rotator); }
            }

            var rotation = _chainItems[chainPosition].AddComponent<RotationComponent>();
            rotation.minRotation = -20f;
            rotation.maxRotation = 20f;
            rotation.rotationSpeed = 45f;
        }
    
        // Start is called before the first frame update
        private void Awake()
        {
            var pool = choiceSet != null ? new ChoicePool(choiceSet.ToChoices()) : null;
            _driver = new ChoiceDriver(pool);

            foreach (var go in choiceButtons)
            {
                var opt = go.GetComponent<ChoiceOptionComponent>();
                if (opt == null) continue;
                _options.Add(opt);
                opt.SetOnChoiceSelected(HandleChoice);
            }

            UpdateOptions();
            UpdateChoiceChain();
        }

        private void HandleChoice(Choice choice)
        {
            _driver.MakeChoice(choice);
            UpdateOptions();
            UpdateChoiceChain();
        }

        private void UpdateChoiceChain()
        {
            var chain = _driver.CurrentChoices();

            var childCount = transform.childCount;
            var chainLength = chain.Length;
            if (childCount >= chainLength) return;
            
            for (var i = childCount; i < chainLength; i++)
            {
                var img = Instantiate(imagePrefab, transform);
                var texture = chain[i].Texture;
                if (texture == null || imagePrefab == null) return;
                
                var imgComp = img.GetComponent<Image>();
                        
                imgComp.sprite = Sprite.Create(
                    texture,
                    new Rect(0, 0, texture.width, texture.height),
                    new Vector2(0.5f, 0.5f)
                );
                _chainItems.Add(img);
            }
        }
        
        private void UpdateOptions()
        {
            var nextOptions = _driver.NextOptions();
            nextOptions.Shuffle();
            for (var i = 0; i < _options.Count; i++)
            {
                var opt = _options[i];
                if (i < nextOptions.Length)
                {
                    opt.gameObject.SetActive(true);
                    opt.SetChoice(nextOptions[i]);
                }
                else
                {
                    opt.gameObject.SetActive(false);
                }
            }
        }
    }
}
