using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChoicePool
{
    private List<Choice> _allChoices;
    private Dictionary<int, bool> _chosenItems = new Dictionary<int, bool>();
    private Dictionary<String, List<Choice>> _inRhymes;

    
    [Obsolete("Use the Constructor which takes in a list of Choices. This version only builds a test set of hardcoded data")]
    public ChoicePool()
    {
        _allChoices = new List<Choice>();

        _allChoices.Add(new Choice()
        {
            Id = 1, Name = "a Mocking Bird", Deficiency = "won't Sing", InRhyme = "-urd", OutRhyme = "-ing", Key = "mockingbird"
        });
        _allChoices.Add(new Choice()
        {
            Id = 2, Name = "a Diamond Ring", Deficiency = "won't Shine", InRhyme = "-ing", OutRhyme = "-ine", Key = "ring"
        });
        _allChoices.Add(new Choice()
        {
            Id = 3, Name = "a Tire Swing", Deficiency = "won't Hold", InRhyme = "-ing", OutRhyme = "-old", Key = "tireswing"
        });
        _allChoices.Add(new Choice()
        {
            Id = 4, Name = "a Metal Spring", Deficiency = "won't Jump", InRhyme = "-ing", OutRhyme = "-ump", Key = "metalspring"
        });
        _allChoices.Add(new Choice()
        {
            Id = 5, Name = "a Porcupine", Deficiency = "pricks You", InRhyme = "-ine", OutRhyme = "-ooo", Key = "porcupine"
        });

        Initialize();
    }

    public ChoicePool(List<Choice> choices)
    {
        _allChoices = choices;
        Initialize();
    }

    public Choice GetRootChoice()
    {
        // Return the Mocking Bird
        return _allChoices[0];
    }

    public void MarkChosen(Choice choice)
    {
        _chosenItems[choice.Id] = true;
    }

    public List<Choice> GetNextOptions(Choice choice)
    {
        if (!_inRhymes.ContainsKey(choice.OutRhyme))
        {
            Debug.Log("NO Rhymes for " + choice.OutRhyme);
            return new List<Choice>();
        }
        
        var choices = _inRhymes[choice.OutRhyme];
        if (choices.Count != 0)
            return choices.Where(c => { return !_chosenItems.ContainsKey(c.Id) || !_chosenItems[c.Id]; }).ToList();
        
        Debug.Log("Empty Rhymes for " + choice.OutRhyme);
        return new List<Choice>();

    }

    private void Initialize()
    {
        _inRhymes = _allChoices
            .GroupBy(c => c.InRhyme)
            .ToDictionary(g => g.Key, g => g.ToList());
    }
}