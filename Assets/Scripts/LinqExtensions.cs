using System.Collections.Generic;
using Unity.Mathematics;

public static class LinqExtensions
{
    private static Random _rng = new Random(uint.MaxValue);  

    public static void Shuffle<T>(this IList<T> list)  
    {  
        var n = list.Count;  
        while (n > 1) {  
            n--;
            var k = _rng.NextInt(n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }       
}