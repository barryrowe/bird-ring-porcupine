using System.Collections.Generic;
using UnityEngine;

public static class CachedResource {
    private static readonly Dictionary<string, Object> Cache = new();
        
    public static void Clear() {
        Cache.Clear();
    }
    public static T Load<T>(string path) where T: Object {
        if (!Cache.ContainsKey(path)) {
            Cache[path] = Resources.Load<T>(path);
        }
        return Cache[path] as T;
    }
}